﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.GameRuller;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;

namespace LifeGame.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            DispatcherHelper.Initialize();
            _map = new ObservableCollection<ObservableCollection<bool>>();
            for (int i = 0; i < 10; i++)
            {
                _map.Add(new ObservableCollection<bool>());
            }
            GameRuller.Instance.InitializeMap();
            GetMap();

            PlayCommand = new RelayCommand(() =>
            {
                GameRuller.Instance.Play();
            });

            GameRuller.Instance.OnTurnEnded += GetMap;
        }
        private ObservableCollection<ObservableCollection<bool>> _map;
        public ObservableCollection<ObservableCollection<bool>> Map
        {
            get
            {
                return _map;
            }
        }

        public ICommand PlayCommand { get; private set; }

        private void GetMap()
        {
            var m = GameRuller.Instance.GetMap();
            DispatcherHelper.CheckBeginInvokeOnUI(
            () =>
            {
                for (int i = 0; i < 10; i++)
                {
                    _map[i].Clear();
                    for (int j = 0; j < 10; j++)
                    {
                        _map[i].Add(m[i, j]);
                    }
                }
            });
        }

    }
}
