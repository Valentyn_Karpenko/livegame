﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Maps;
using System.Threading;

namespace Models.GameRuller
{
    public class GameRuller
    {
        #region Singleton
        private static GameRuller _instance = null;

        public static GameRuller Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GameRuller();
                return _instance;
            }
        }

        private GameRuller()
        {
            Map = new GameMap(10);
        }
        #endregion

        private GameMap Map { get; set; }

        public delegate void TurnEndHandler();

        public event TurnEndHandler OnTurnEnded;

        public void InitializeMap()
        {
            Random rand = new Random();
            foreach (var leaf in Map)
            {
                if (rand.Next() % 2 == 0)
                {
                    leaf.Data.Born();
                    leaf.Data.ApplyNextTurn();
                }
            }
        }

        public void MakeTurn()
        {
            foreach (var leaf in Map)
            {
                int aliveNeighbors = leaf.Neighbors.Count(n => n.Data.Alive);
                if (aliveNeighbors == leaf.Data.NeighborsToBorn)
                    leaf.Data.Born();
                else if (aliveNeighbors > leaf.Data.MaximumNeighborsToLive || aliveNeighbors < leaf.Data.MinimumNeighborsToLive)
                    leaf.Data.Die();
            }

            foreach (var leaf in Map)
            {
                leaf.Data.ApplyNextTurn();
            }

            if (OnTurnEnded != null)
                OnTurnEnded.Invoke();
        }

        public void Play()
        {
            Thread playThread = new Thread(Playing);
            playThread.Name = "playThread";
            playThread.Start();
        }

        private void Playing()
        {
            while (true)
            {
                MakeTurn();
                Thread.Sleep(100);
            }
        }

        public bool[,] GetMap()
        {
            bool[,] result = new bool[Map.MapSize, Map.MapSize];
            int counter = 0;
            foreach (var leaf in Map)
            {
                result[counter / Map.MapSize, counter % Map.MapSize] = leaf.Data.Alive;
                counter++;
            }
            return result;
        }
    }
}
