﻿using Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Models.Octree;

namespace Models.Maps
{
    class GameMap : IEnumerable<ILeaf<ICell>>
    {
        public int MapSize { get; set; }
        private ILeaf<ICell> _startingLeaf;
        public IEnumerator<ILeaf<ICell>> GetEnumerator()
        {
            return new MapEnumerator(MapSize, _startingLeaf);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public GameMap(int mapSize)
        {
            MapSize = mapSize;
            CreateMap();
        }

        private void CreateMap()
        {
            _startingLeaf = new Leaf<ICell>(new Cell());
            ILeaf<ICell> currentLeaf = _startingLeaf;
            for (int i = 0; i < MapSize - 1; i++)
            {
                currentLeaf[Sides.Right] = new Leaf<ICell>(new Cell());
                currentLeaf = currentLeaf[Sides.Right];
            }
            _startingLeaf[Sides.Left] = currentLeaf;
            ILeaf<ICell> topLeaf = _startingLeaf;

            for (int i = 0; i < MapSize - 1; i++)
            {
                currentLeaf = new Leaf<ICell>(new Cell());
                currentLeaf[Sides.TopLeft] = topLeaf[Sides.Left];
                currentLeaf[Sides.Top] = topLeaf;
                currentLeaf[Sides.TopRight] = topLeaf[Sides.Right];

                ILeaf<ICell> lineStartingLeaf = currentLeaf;

                for (int j = 0; j < MapSize - 1; j++)
                {
                    currentLeaf[Sides.Right] = new Leaf<ICell>(new Cell());
                    currentLeaf = currentLeaf[Sides.Right];
                    topLeaf = topLeaf[Sides.Right];
                    currentLeaf[Sides.TopLeft] = topLeaf[Sides.Left];
                    currentLeaf[Sides.Top] = topLeaf;
                    currentLeaf[Sides.TopRight] = topLeaf[Sides.Right];
                }
                lineStartingLeaf[Sides.Left] = currentLeaf;
                topLeaf = lineStartingLeaf;
            }

            currentLeaf = _startingLeaf;

            for (int i = 0; i < MapSize; i++)
            {
                currentLeaf[Sides.TopLeft] = topLeaf[Sides.Left];
                currentLeaf[Sides.Top] = topLeaf;
                currentLeaf[Sides.TopRight] = topLeaf[Sides.Right];
                currentLeaf = currentLeaf[Sides.Right];
                topLeaf = topLeaf[Sides.Right];
            }


        }
    }
}
