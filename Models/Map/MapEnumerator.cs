﻿using Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Models.Maps
{
    class MapEnumerator : IEnumerator<ILeaf<ICell>>
    {
        public int MapSize { get; private set; }
        private int _widthIterator = -1;
        private int _heightIterator = 0;
        private ILeaf<ICell> _startingPoint;
        private ILeaf<ICell> _current;

        public MapEnumerator(int mapSize, ILeaf<ICell> startingPoint)
        {
            MapSize = mapSize;
            this._startingPoint = startingPoint;
            this._current = startingPoint[Sides.Left];
        }

        public ILeaf<ICell> Current
        {
            get
            {
                return _current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            _widthIterator++;
            if(_widthIterator >= MapSize)
            {
                _widthIterator = 0;
                _heightIterator++;
                _current = _current[Sides.BottomRight];
            }
            else
            {
                _current = _current[Sides.Right];
            }
            return _heightIterator < MapSize;
        }

        public void Reset()
        {
            _current = _startingPoint;
            _widthIterator = -1;
            _heightIterator = 0;
        }
    }
}
