﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Interfaces;

namespace Models.Maps
{
    public class Cell : ICell
    {
        public bool Alive { get; private set; }
        private bool nextTurnStatus;
        public int MinimumNeighborsToLive { get; private set; } = 2;
        public int MaximumNeighborsToLive { get; private set; } = 3;
        public int NeighborsToBorn { get; private set; } = 2;

        public void Born()
        {
            nextTurnStatus = true;
        }

        public void Die()
        {
            nextTurnStatus = false;
        }

        public void ApplyNextTurn()
        {
            Alive = nextTurnStatus;
        }
    }
}
