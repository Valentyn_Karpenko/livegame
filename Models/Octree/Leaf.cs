﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Interfaces;

namespace Models.Octree
{
    class Leaf<T> : ILeaf<T>
    {
        public ILeaf<T>[] Neighbors { get; private set; }

        public T Data { get; set; }

        public ILeaf<T> this[Sides index]
        {
            get
            {
                return this[(int)index];
            }

            set
            {
                this[(int)index] = value;
                var reverseIndex = ReverseDirection(index);
                if (value != null && value[reverseIndex] != this)
                {
                    value[reverseIndex] = this;
                }
            }
        }

        private static Sides ReverseDirection(Sides side)
        {
            int reverseSideNumber = 7 - (int)side;
            return (Sides)reverseSideNumber;
        }

        public ILeaf<T> this[int index]
        {
            get
            {
                if (index > 8 || index < 0)
                    throw new IndexOutOfRangeException("Index must be betwean 0 and 8");
                return Neighbors[index];
            }

            set
            {
                if (index > 8 || index < 0)
                    throw new IndexOutOfRangeException("Index must be betwean 0 and 8");
                Neighbors[index] = value;
            }
        }

        public Leaf()
        {
            Neighbors = new Leaf<T>[8];
        }
        public Leaf(T data) : this()
        {
            Data = data;
        }
    }
}
