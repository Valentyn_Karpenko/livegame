﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Interfaces
{
    interface ILeaf<T>
    {
        ILeaf<T>[] Neighbors { get; }

        T Data { get; set; }

        ILeaf<T> this[int index] { get; set; }

        ILeaf<T> this[Sides index] { get; set; }
    }

    enum Sides
    {
        TopLeft = 0,
        Top,
        TopRight,
        Left,
        Right,
        BottomLeft,
        Bottom,
        BottomRight
    }
}
