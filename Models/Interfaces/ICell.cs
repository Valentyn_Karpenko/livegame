﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Interfaces
{
    interface ICell
    {
        bool Alive { get; }
        int MinimumNeighborsToLive { get; }
        int MaximumNeighborsToLive { get; }
        int NeighborsToBorn { get; }
        void Born();
        void Die();
        void ApplyNextTurn();
    }
}
